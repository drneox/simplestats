from csv import reader
from collections import defaultdict


class Stats:
    def __init__(self, delimiter, data='data.csv', head=None):
        self.delimiter = delimiter
        self.data = data
        self.head = head
        self.count = 0

    def load(self):
        read = reader(open(self.data, 'rb'))
        all_data = []
        for index, row in enumerate(read):
            all_data.append(row[0].split(self.delimiter))
        if self.head:
            all_data = all_data[1:]
        self.count = len(all_data)
        return all_data

    def load_row(self, value):
        list_row = []
        if self.head:
            index = self.load()
            print index
        else:
            index = value
        self.load()
        for row in self.load():
            list_row.append(row)
        return list_row

    def frequency(self, index):
        result = defaultdict(int)
        for i in self.load_row(index):
            result[i] += 1
        for key, value in result.items():
            print  key, '--------------------', value
        print "total: %d" % self.count



